/**
 * Grunt: Mehde und Banse.
 */

module.exports = function (grunt) {

  'use strict';

  // Load configuration.
  require('load-grunt-config')(grunt, {
    configPath: [
      process.cwd() + '/.grunt/config'
    ],
    data: {
      path: {
        _self: process.cwd(),
        cache: '.cache',
        dist: {
          _self: 'files'
        },
        grunt: {
          config: '.grunt'
        },
        src: {
          components: 'src/components',
          sass: 'src/scss',
          svgs: 'src/svgs'
        },
        tmp: {
          _self: '.tmp',
          cache: '.tmp/cache',
          css: {
            autoprefixed: '.tmp/css/autoprefixed',
            compiled: '.tmp/css/compiled',
            compressed: '.tmp/css/compressed',
            minified: '.tmp/css/minified'
          },
          sass: {
            globbing: '.tmp/scss/globbing'
          },
          svgs: {
            minified: '.tmp/svgs/minified'
          }
        }
      }
    },
    init: true,
    jitGrunt: {
      staticMappings: {
        sasslint: 'grunt-sass-lint'
      }
    }
  });

};
