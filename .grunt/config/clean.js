/**
 * Grunt task: Clean files/directories.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Cache files.
    cache: ['<%= path.cache %>/'],

    // Distribution files.
    dist: [
      '<%= path.dist._self %>/application.css',
      '<%= path.dist._self %>/_svg.*.html'
    ],

    // Options.
    options: {
      force: true
    },

    // Temporary files.
    tmp: ['<%= path.tmp._self %>/']
  };
};
