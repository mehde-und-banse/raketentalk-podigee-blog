/**
 * Grunt task: Automatically add CSS vendor prefixes.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Options.
    options: {
      browsers: [
        'last 2 versions',
        'ie 9',
        'ie 10',
        'ie 11'
      ],
      cascade: false
    },

    // Styles.
    styles: {
      files: [
        {
          cwd: '<%= path.tmp.css.compiled %>/',
          dest: '<%= path.tmp.css.autoprefixed %>/',
          expand: true,
          src: ['**/*.css']
        }
      ]
    }
  };
};
