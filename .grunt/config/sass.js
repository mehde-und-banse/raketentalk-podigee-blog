/**
 * Grunt task: Compile Sass to CSS.
 */

module.exports = function (grunt, options) {

  'use strict';

  var sass = require('node-sass');

  return {
    // Options.
    options: {
      implementation: sass,
      outputStyle: 'expanded',
      precision: 10,
      sourceMap: false
    },

    // Styles.
    styles: {
      files: {
        '<%= path.tmp.css.compiled %>/application.css': '<%= path.src.sass %>/application.scss'
      }
    }
  };
};
