/**
 * Grunt task: Perform tasks concurrently.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Compile all assets.
    compile: [
      'compile.styles',
      'compile.svgs'
    ],

    // Options.
    options: {
      limit: 8
    }
  };
};
