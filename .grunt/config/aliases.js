/**
 * Grunt aliases.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Perform build.
    build: [
      'clean:cache',
      'clean:tmp',
      'clean:dist',
      'test.scripts',
      'test.styles',
      'concurrent:compile',
      'copy.dist'
    ],

    // Compile styles.
    'compile.styles': [
      'sass_globbing',
      'sass:styles',
      'changed:autoprefixer',
      'changed:cssmin'
    ],

    // Compile SVGs.
    'compile.svgs': [
      'changed:svgmin'
    ],

    // Copy distribution files.
    'copy.dist': [
      'changed:copy:dist'
    ],

    // Default task.
    default: [
      'build',
      'watch'
    ],

    // Test scripts.
    'test.scripts': [
      'changed:eslint:grunt'
    ],

    // Test styles.
    'test.styles': [
      'changed:sasslint'
    ]
  };
};
