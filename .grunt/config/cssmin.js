/**
 * Grunt task: Minify CSS files.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Options.
    options: {
      advanced: true,
      aggressiveMerging: true,
      keepBreaks: false,
      keepSpecialComments: 0,
      mediaMerging: true,
      processImport: true,
      rebase: false,
      report: 'gzip',
      restructuring: true,
      roundingPrecision: 4,
      semanticMerging: false,
      shorthandCompacting: true,
      sourceMap: false
    },

    // Styles.
    styles: {
      files: [
        {
          cwd: '<%= path.tmp.css.autoprefixed %>/',
          dest: '<%= path.tmp.css.minified %>/',
          expand: true,
          ext: '--min.css',
          src: ['**/*.css']
        }
      ]
    }
  };
};
