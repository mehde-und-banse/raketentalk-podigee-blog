/**
 * Grunt task: Only process newer stuff.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Options.
    options: {
      cache: '<%= path.cache %>/changed'
    }
  };
};
