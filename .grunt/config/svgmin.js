/**
 * Grunt task: Minify SVG files.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Options.
    options: {
      plugins: [
        {addClassesToSVGElement: false},
        {cleanupAttrs: true},
        {cleanUpEnableBackground: true},
        {cleanupIDs: true},
        {cleanupListOfValues: true},
        {cleanupNumericValues: true},
        {collapseGroups: true},
        {convertColors: true},
        {convertPathData: true},
        {convertShapeToPath: true},
        {convertStyleToAttrs: true},
        {convertTransform: true},
        {mergePaths: true},
        {minifyStyles: true},
        {moveElemsAttrsToGroup: true},
        {moveGroupAttrsToElems: true},
        {removeAttrs: {
          attrs: [
            'data-name'
          ]
        }},
        {removeComments: true},
        {removeDesc: true},
        {removeDimensions: false},
        {removeDoctype: true},
        {removeEditorsNSData: true},
        {removeEmptyContainers: true},
        {removeEmptyAttrs: true},
        {removeEmptyText: true},
        {removeHiddenElems: true},
        {removeMetadata: true},
        {removeNonInheritableGroupAttrs: true},
        {removeRasterImages: true},
        {removeStyleElement: false},
        {removeTitle: true},
        {removeUnknownsAndDefaults: true},
        {removeUnusedNS: true},
        {removeUselessDefs: true},
        {removeUselessStrokeAndFill: true},
        {removeViewBox: false},
        {removeXMLProcInst: true},
        {sortAttrs: true},
        {transformsWithOnePath: true}
      ]
    },

    // SVGs.
    svgs: {
      files: [
        {
          cwd: '<%= path.src.svgs %>/',
          dest: '<%= path.tmp.svgs.minified %>/',
          expand: true,
          src: ['**/*.svg']
        }
      ]
    }
  };
};
