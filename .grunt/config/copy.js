/**
 * Grunt task: Copy files/directories.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Distribution files.
    dist: {
      files: [
        // Styles.
        {
          dest: '<%= path.dist._self %>/application.css',
          src: '<%= path.tmp.css.minified %>/application--min.css'
        },

        // SVGs.
        {
          cwd: '<%= path.tmp.svgs.minified %>/',
          dest: '<%= path.dist._self %>/',
          expand: true,
          filter: 'isFile',
          rename: function (dest, src) {
            return dest + '_svg.' + src.replace('.svg', '.html');
          },
          src: ['**/*.svg']
        }
      ]
    }
  };
};
