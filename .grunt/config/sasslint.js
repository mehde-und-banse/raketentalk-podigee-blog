/**
 * Grunt task: Perform SassLint checks.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Options.
    options: {
      configFile: '<%= path._self %>/.sass-lint.yml'
    },

    // Styles.
    styles: [
      '<%= path.src.components %>/**/*.scss',
      '<%= path.src.sass %>/**/*.scss'
    ]
  };
};
