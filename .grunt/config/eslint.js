/**
 * Grunt task: ESLint.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Grunt.
    grunt: [
      '<%= path._self %>/Gruntfile.js',
      '<%= path.grunt.config %>/**/*.js'
    ],

    // Options.
    options: {
      configFile: '<%= path._self %>/.eslintrc',
      ignore: false
    }
  };
};
