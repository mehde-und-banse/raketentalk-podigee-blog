/**
 * Grunt task: Watch files to perform certain tasks.
 */

module.exports = function (grunt, options) {

  'use strict';

  return {
    // Grunt.
    grunt: {
      files: [
        '<%= path._self %>/.eslintrc',
        '<%= path._self %>/Gruntfile.js',
        '<%= path.grunt.config %>/**/*.js'
      ],
      options: {
        reload: true
      },
      tasks: ['changed:eslint:grunt']
    },

    // Sass.
    sass: {
      files: [
        '<%= path._self %>/.sass-lint.yml',
        '<%= path.src.components %>/**/*.scss',
        '<%= path.src.sass %>/**/*.scss'
      ],
      tasks: [
        'test.styles',
        'compile.styles',
        'copy.dist'
      ]
    },

    // SVGs.
    svgs: {
      files: ['<%= path.src.svgs %>/**/*.svg'],
      tasks: [
        'compile.svgs',
        'copy.dist'
      ]
    }
  };
};
