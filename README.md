# Raketentalk Podigee Blog Theme

The top-level `files` directory is mandatory for a successful import of the theme files.

You'll find more detailed instructions here:

* https://help.podigee.com/article/160-importing-themes-from-git-repos
* https://help.podigee.com/article/162-the-theme-editor
* https://help.podigee.com/article/161-available-theme-template-variables

Information about Liquid template engine (that is used by Podigee):

* https://shopify.github.io/liquid/
